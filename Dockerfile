ARG UMAP_VERSION=2.9.0
FROM umap/umap:$UMAP_VERSION
ARG UMAP_DSFR_VERSION=2025.03.03

RUN apt update && apt install -y git && rm -rf /var/lib/apt/lists/*
RUN pip install git+https://github.com/umap-project/umap-dsfr@$UMAP_DSFR_VERSION

COPY umap.conf /etc/umap/umap.conf
