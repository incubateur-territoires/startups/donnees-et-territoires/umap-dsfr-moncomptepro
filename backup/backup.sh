#!/bin/bash

set -xeu

check_vars()
{
    set +u
    var_names=("$@")
    var_unset=0
    for var_name in "${var_names[@]}"; do
        [ -z "${!var_name}" ] && echo "$var_name is unset." && var_unset=1
    done
    set -u
    return  "$var_unset"
}

check_vars \
    POSTGRESQL_HOST POSTGRESQL_PASSWORD POSTGRESQL_PORT POSTGRESQL_USER POSTGRESQL_DATABASE \
    S3_ACCESS_KEY S3_BUCKET_NAME S3_ENDPOINT_URL S3_REGION S3_SECRET_KEY
# Check that the volume is actually mounted
if [ -f /data/.volume_mount_canary ]
then
  echo "Volume not mounted on /data."
  exit 1
fi

# Config

DATE=$(date +'%Y%m%d%H%M')

aws configure set plugins.endpoint awscli_plugin_endpoint

cat > ~/.aws/config <<EOF
[plugins]
endpoint = awscli_plugin_endpoint

[default]
region = $S3_REGION
s3 =
  endpoint_url = $S3_ENDPOINT_URL
  signature_version = s3v4
  max_concurrent_requests = 100
  max_queue_size = 1000
  multipart_threshold = 50MB
  multipart_chunksize = 10MB
s3api =
  endpoint_url = $S3_ENDPOINT_URL
EOF

cat > ~/.aws/credentials <<EOF
[default]
aws_access_key_id=$S3_ACCESS_KEY
aws_secret_access_key=$S3_SECRET_KEY
EOF

mkdir -p /tmp/backup/{content,database}

# Backup DB

PGPASSWORD="$POSTGRESQL_PASSWORD" pg_dump -h $POSTGRESQL_HOST -p $POSTGRESQL_PORT -U $POSTGRESQL_USER -d $POSTGRESQL_DATABASE -n public > /tmp/backup/database/dump.sql

# Backup local files

rsync -ap --no-links --exclude="lost+found" /data/ /tmp/backup/content/

# Archive

tar -zcvf /tmp/$DATE.tar.gz -C /tmp/backup .
tar -tzf /tmp/$DATE.tar.gz >/dev/null

# Upload

aws s3 cp /tmp/$DATE.tar.gz s3://$S3_BUCKET_NAME
